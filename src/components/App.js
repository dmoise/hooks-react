import React, { useState } from 'react';
import ResourceList from './ResourceList';
import UserList from './UserList';


const App = () => {
    const [resource, setResource] = useState('posts');
    //const [blog, setBlog] = useState('asa');
    return (
        <div className="ui container" style={{ marginTop: '10px' }}>
            <UserList/>
            <div className="ui container">
                <button onClick={() => setResource('posts')} className="ui button primary">Posts</button>
                <button onClick={() => setResource('todos')} className="ui button red">Todos</button>
            </div>
            <ResourceList resource={resource}/>
        </div>
    );
};

export default App;